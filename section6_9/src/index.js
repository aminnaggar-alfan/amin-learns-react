import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware, compose } from 'redux';
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom';
import reduxPromise from 'redux-promise';
import { composeWithDevTools } from 'redux-devtools-extension';

import reducers from './reducers';
import NoMansGround from './components/404';
import PostsIndex from './components/posts_index';
import PostsNew from './components/posts_new';
import PostsShow from './components/posts_show';

// const createStoreWithMiddleware = applyMiddleware(reduxPromise)(createStore);
// <Provider store={createStoreWithMiddleware(reducers)}>

const store = createStore(reducers, composeWithDevTools( applyMiddleware(reduxPromise) ));

ReactDOM.render(
  <Provider store={store}>
    <BrowserRouter>
      <div>
        <Switch>
          <Route exact path='/posts' component={PostsIndex}/>
          <Route path='/posts/new' component={PostsNew}/>
          <Route path='/posts/:id' component={PostsShow}/>
          <Route path='/404' component={NoMansGround}/>
          <Route exact from='/' component={PostsIndex}/>
          <Redirect from='*' to='/404'/>
        </Switch>
      </div>
    </BrowserRouter>
  </Provider>
  , document.querySelector('.container'));
