import React, {Component} from 'react';
import { Field, reduxForm } from 'redux-form';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { createPost } from '../actions';

const FIELDS = {
  title: {
    label: 'Post Title*',
    type: 'input'
  },
  categories: {
    label: 'Categories*',
    type: 'input'
  },
  content: {
    label: 'Post Contents*',
    type: 'textarea'
  },
};

class PostsNew extends Component {

  renderField(field, fieldConfig) {
    const { meta: {touched, error} } = field;
    const className = `form-group ${touched && error ? 'has-error' : '' }`

    return (
      <div className={className}>
        <label>{fieldConfig.label}</label>
        <fieldConfig.type
          className='form-control'
          {...field.input}
        />
        <div className='text-danger'>
          {touched ? error : ''}
        </div>
      </div>
    );
  }

  onSubmit(values) {
    this.props.createPost(values, () => {
      this.props.history.push('/');
    });
  }

  render() {
    const { handleSubmit } = this.props;

    return (
      <form onSubmit={ handleSubmit(this.onSubmit.bind(this)) }>
        {
          _.map(FIELDS, (item, key) => {
            return <Field key={key} name={key}
              component={field => this.renderField(field, item)}
            />
          })
        }

        <button type="submit" className="btn btn-primary">Submit</button>
        <Link to='/' className='btn btn-danger'>Cancel</Link>
      </form>
    );
  }
}

function validate(values) {
  const errors = {};

  _.each(FIELDS, (type, field) => {
    if (!values[field]) errors[field] = `Please enter a ${field}`;
  })

  // if we return an empty object, the form is fine to submit
  return errors;
}

export default reduxForm({
  validate,    // the validate function
  form: 'PostsNewForm'    // this is what we're gonna call our form as an id key
})(
   connect(null, { createPost })(PostsNew)
);
