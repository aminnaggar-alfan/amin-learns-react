import React, {Component} from 'react';
import { connect } from 'react-redux';
import Chart from '../components/chart';
import GoogleMap from '../components/google_map'

class WeatherList extends Component {

  renderWeather(cityData) {
    const temps = cityData.list.map(data => data.main.temp - 273);    // -273 from Kelvin to Celsius
    const pressures = cityData.list.map(data => data.main.pressure);
    const humidities = cityData.list.map(data => data.main.humidity);
    const {lon, lat} = cityData.city.coord;

    return (
      <tr key={cityData.city.id}>
        <td><GoogleMap lon={lon} lat={lat} /></td>
        <td><Chart data={temps} units='C' colour='red'/></td>
        <td><Chart data={pressures} units='hPa' colour='orange'/></td>
        <td><Chart data={humidities} units='%' colour='blue'/></td>
      </tr>
    );
  }

  render() {
    return (
      <table className='table table-hover'>
        <thead>
          <tr>
            <th>City</th>
            <th>Temperature (C)</th>
            <th>Pressure (hPa)</th>
            <th>Humidity (%)</th>
          </tr>
        </thead>
        <tbody>
          {this.props.weather.map(this.renderWeather)}
        </tbody>
      </table>
    )
  }
}

function mapStateToProps({ weather }) {
  return { weather };
}

// const mapStateToProps = ({weather} => {weather});

export default connect(mapStateToProps)(WeatherList);
