import _ from 'lodash';
import React from 'react';
import {
  Sparklines,
  SparklinesLine,
  SparklinesReferenceLine,
  SparklinesSpots} from 'react-sparklines';

const average = data => _.round(_.mean(data));

export default ({data, colour, units}) => {
  const width = 180
  const height = 120;
  return (
    <div>
      <Sparklines
        data={data}
        width={width}
        height={height}>
        <SparklinesLine color={colour}/>
        <SparklinesReferenceLine type='avg'/>
        <SparklinesSpots style={{ fill: colour }}/>
      </Sparklines>
      <div>{average(data)} {units}</div>
    </div>
  )
};
