export default function() {
  return [
    {title: 'Javascript', pages: 42},
    {title: 'Harry Potter', pages: 4},
    {title: 'The Dark Lord', pages: 2},
    {title: 'Eloquent Yew', pages: 1},
  ];
};
